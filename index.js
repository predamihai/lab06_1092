const express=require("express");
const app=express();
const bodyParser=require("body-parser");

app.use(bodyParser.json());
app.locals.todos=[];

app.get("/todos",(request,response)=>{
   response.status(200).send(app.locals.todos); 
});

app.get("/",(request,response)=>{
    response.status(200).send({
        message: 'My first message'
    });
})

app.post("/todos",(request,response)=>{
   const todo=request.body;
   if(todo.taskName && todo.duration && todo.priority)
   {
       app.locals.todos.push(todo);
       response.status(201).send({
          message:'Todo added succesfully' 
       });
   }
   else
   {
       response.status(400).send({
          message:"Invalid todo payload" 
       });
   }
});

app.listen(8080,()=>{
    console.log("Server started on port 8080.....");
});